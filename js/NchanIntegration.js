class Nchan {

    constructor(api, websocket, outputId) {
        this.webSocket = new WebSocket(websocket);
        this.api = api;
        this.outputElement = document.getElementById(outputId);
        this.webSocket.onmessage = this.handleMessage.bind(this);
        this.buttons = [];

        let buttonCollection = document.getElementsByClassName("nchan-click");
        for(const button of buttonCollection) {
            const nchanButton = new NchanButton(button);
            this.buttons.push(nchanButton);
        }
    }

    getButton(deviceId, commandId) {
        return this.buttons.filter(button => button.deviceId == deviceId && button.commandId == commandId)
    }

    handleResponse(response) {
        const buttons = this.getButton(response.deviceId, response.commandId)
        buttons.forEach(button => button.setStatus(response.status));
    }

    handleMessage(e) {
        console.log("NChan MessageQueue Event", e);
        const message = JSON.parse(e.data);
        if(message.type === "response") {
            this.handleResponse(message);
        }

        this.outputMessage(e.data);
    }

    outputMessage(message) {
        if(this.outputElement === undefined) return 
        const paragraph = document.createElement("p");
        paragraph.append(message);
        this.outputElement.prepend(paragraph);
    }

}

class NchanButton {

    constructor(buttonElement) {
        if(buttonElement.dataset == undefined) return
        this.element = buttonElement;
        this.blocked = false;
        this.state = "ready";
        this.commandId = buttonElement.dataset.commandId;
        this.deviceId = buttonElement.dataset.deviceId;
        this.states = JSON.parse(buttonElement.dataset.states);
        this.element.onclick = this.handleClick.bind(this);
        this.clickSound = new Audio('https://webwork18.math.uzh.ch/semlive/fileadmin/SeminarLive/touch/click.mp3');
        console.log(this);
    }

    setStatus(status) {
        this.state = status;
        for (const state in this.states) {
            if(state === status) {
                this.element.classList.add(this.states[state]);
            } else if(this.element.classList.contains(this.states[state])) {
                this.element.classList.remove(this.states[state]);
            }
        }

        if(status === "waiting") {
            this.blocked = true;
            this.element.disabled = true;
        } else {
            this.blocked = false;
            this.element.disabled = false;
        }
    }

    handleClick(e) {
        if(this.blocked) return

        this.clickSound.play();
       
        const formData = new FormData();
        formData.append("action", ((this.state == "running") ? "stop" : "start"));

        fetch(this.element.dataset.api, {
            method: "POST",
            body: formData
        })
        .then(response => {
            console.log("Response", response);
        })
        .catch((error) => {
            console.error('Error:', error);
        });
        this.setStatus("waiting");
    }
}

window.onload = () => {
    const nchanInit = document.getElementsByClassName("nchan-init");
    for(let config of nchanInit) {
        const nchan = new Nchan(config.dataset.api, config.dataset.nchan, config.dataset.outputId)
    }
}
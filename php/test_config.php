<?php
    include("config.php");
    $roomName = "TestRoomz";
    $dbh = new PDO("mysql:host=" . $config["host"] . ";dbname=" . $config["dbname"], $config["user"], $config["password"]);
    $statement = $dbh->prepare('SELECT * from Room AS r WHERE r.name = :room');
    $statement->bindValue(':room', $roomName, PDO::PARAM_STR);
    $statement->execute();
    $room = $statement->fetch(PDO::FETCH_ASSOC);
    print_r($room);
    
?>

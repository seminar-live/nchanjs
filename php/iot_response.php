<?php
    include("config.php");
    $json = file_get_contents('php://input');
    $data = json_decode($json);
    if(isset($config["log"])) { $fp = fopen($config["log"], 'a'); }
    $dbh = new PDO("mysql:host=" . $config["host"] . ";dbname=" . $config["dbname"], $config["user"], $config["password"]);
    $statement = $dbh->prepare('SELECT * from Room AS r WHERE r.name = :room');
    $statement->bindValue(':room', $data->room, PDO::PARAM_STR);
    $statement->execute();
    $room = $statement->fetch(PDO::FETCH_ASSOC);
    $dbh = null;
    
    if($data->roomKey == $room["roomToken"]) {
        $url = 'http://localhost:8080/semlive_iot_pub/' . $data->room;
        $message = array(
                "type" => "response",
                "deviceId" => $data->deviceId,
                "commandId" => $data->commandId,
                "status" => $data->status,
                "message" => $data->message
        );

        $options = array(
                'http' => array(
                        'header' => "Content-type: application/json",
                        'method' => 'POST',
                        'content' => json_encode($message)
                )
        );
        $context = stream_context_create($options);
        $result = file_get_contents($url, false, $context);
        if($result == FALSE) {
            print('{"status": "error", "message": "Could not send response to NChan"}');
            if(isset($config["log"])) {
                fwrite($fp, "--------" . PHP_EOL . "[Error]" . PHP_EOL);
                fwrite($fp, "Response: " . json_encode($message) . PHP_EOL);
                fwrite($fp, print_r($room, true));
                fwrite($fp, print_r($data, true));
            }
        } else {
            print('{"status": "success", "message": "Sent response"}');    
            if(isset($config["log"])) { fwrite($fp, "Response: " . json_encode($message) . PHP_EOL); }
        }
    } else {
        print('{"status": "error", "message": "invalid roomkey"}');
    }
    if(isset($config["log"])) { fclose($fp); }
?>
